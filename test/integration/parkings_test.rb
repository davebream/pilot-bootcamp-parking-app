require 'test_helper'

class ParkingsTest < ActionDispatch::IntegrationTest
  def setup
    test_sign_in
  end

  test 'user opens parkings index' do
    visit parkings_path
    assert has_content? 'Parkings'
  end

  test 'user opens parking details' do
    visit parkings_path

    within first '.parking-row' do
      click_link 'Show'
    end

    assert has_content? 'Parking details'
    assert has_content? 'Rzeszów'
    assert has_content? '13.0'
    assert has_content? '133.0'
  end

  test 'user adds a new parking' do
    visit new_parking_path

    fill_in 'parking[places]', with: 123
    select 'Private', from: 'parking[kind]'
    fill_in 'parking[hour_price]', with: 13
    fill_in 'parking[day_price]', with: 123
    fill_in 'parking[address_attributes][city]', with: 'Kraków'
    fill_in 'parking[address_attributes][street]', with: 'Wrocławska 12'
    fill_in 'parking[address_attributes][zip_code]', with: '33-012'
    click_button 'Save'

    within '.pagination' do
      click_on '2'
    end
    assert has_content? 'Parkings'
    assert has_content? 'Kraków'
    assert has_content? '123'
    assert has_content? '13'
    assert has_content? '123'
  end

  test 'user edits a parking' do
    visit parkings_path

    within first '.parking-row' do
      click_link 'Edit'
    end

    fill_in 'parking[places]', with: 120
    select 'Indoor', from: 'parking[kind]'
    fill_in 'parking[hour_price]', with: 20
    fill_in 'parking[day_price]', with: 220
    fill_in 'parking[address_attributes][city]', with: 'Łódź'
    fill_in 'parking[address_attributes][street]', with: 'Inowrocławska 9'
    fill_in 'parking[address_attributes][zip_code]', with: '44-012'
    click_button 'Save'

    assert has_content? 'Parkings'
    assert has_content? 'Łódź'
    assert has_content? '120'
    assert has_content? '20'
    assert has_content? '220'
  end

  test 'user removes a parking' do
    visit parkings_path

    within first '.parking-row' do
      click_link 'Remove'
    end

    assert has_content? 'Parkings'
    assert_not has_content? 'Rzeszów'
    assert_not has_content? '11.0'
    assert_not has_content? '123.0'
  end

  test "data stays in the form when it’s submitted" do
    visit parkings_path

    check 'private'
    check 'public'
    fill_in 'day_price_from', with: 55
    fill_in 'day_price_to', with: 65
    fill_in 'hour_price_from', with: 9
    fill_in 'hour_price_to', with: 11
    fill_in 'city', with: 'Wrocław'
    click_button 'Search'

    assert has_checked_field? :private
    assert has_checked_field? :public
    assert has_field? 'day_price_from', with: 55
    assert has_field? 'day_price_to', with: 65
    assert has_field? 'hour_price_from', with: 9
    assert has_field? 'hour_price_to', with: 11
    assert has_field? 'city', with: 'Wrocław'

    within first '.parking-row' do
      assert has_content? '20'
      assert has_content? '10.0'
      assert has_content? '60.0'
      assert has_content? 'Wrocław'
    end
  end
end
