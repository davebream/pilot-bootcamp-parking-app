require 'test_helper'

class PlaceRentsTest < ActionDispatch::IntegrationTest
  def setup
    test_sign_in
  end

  test 'user rents a parking place' do
    start_date = DateTime.new(2015, 10, 12, 7, 0, 0)
    end_date = DateTime.new(2015, 10, 15, 12, 0, 0)

    visit parkings_path
    assert has_content? 'Parkings'

    within first '.parking-row' do
      click_link 'Rent a place'
    end

    assert has_content? 'Rent a place'
    select_date_and_time start_date, 'place_rent_starts_at'
    select_date_and_time end_date, 'place_rent_ends_at'
    select 'Ford Mondeo', from: 'place_rent[car_id]'
    click_button 'Save'

    assert has_content? 'Place rent details'
    assert has_content? '2015-10-12 07:00:00 UTC'
    assert has_content? '2015-10-15 12:00:00 UTC'
  end

  test 'place rent index displays place rent price' do
    visit place_rents_path

    assert has_content? 'Place Rents'
    assert has_content? '2015-07-08 07:00:00 UTC'
    assert has_content? '2015-07-12 12:26:00 UTC'
    assert has_content? '300.0'
  end

  test 'place rent price should not change when parking price changes' do
    Parking.first.update_attributes(day_price: 70, hour_price: 14)

    visit place_rents_path

    assert has_content? 'Place Rents'
    assert has_content? '2015-07-08 07:00:00 UTC'
    assert has_content? '2015-07-12 12:26:00 UTC'
    assert has_content? '300.0'
  end

  private

  def select_date_and_time(date, field)
    select date.strftime('%Y'), from: "#{field}_1i" # year
    select date.strftime('%B'), from: "#{field}_2i" # month
    select date.strftime('%d'), from: "#{field}_3i" # day
    select date.strftime('%H'), from: "#{field}_4i" # hour
    select date.strftime('%M'), from: "#{field}_5i" # minute
  end
end
