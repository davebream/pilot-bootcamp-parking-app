require 'test_helper'

class CarsTest < ActionDispatch::IntegrationTest
  def setup
    test_sign_in
  end

  test 'user opens cars index' do
    visit cars_path
    assert has_content? 'Cars'
  end

  test 'user opens car details' do
    visit cars_path

    within first '.car-row' do
      click_link 'Show'
    end

    assert has_content? 'Car details'
    assert has_content? 'Ford Mondeo'
    assert has_content? 'DW 5234'
  end

  test 'user adds a new car' do
    visit new_car_path

    car_fill_form
    attach_file('car[image]', File.absolute_path('./test/fixtures/files/toyota_small.jpg'))
    click_button 'Save'

    assert has_content? 'Cars'
    assert has_content? 'Audi'
    assert has_content? 'WA 6655'
    page.has_image?(src: './test/fixtures/files/toyota_small.jpg')
  end

  test 'user edits a car' do
    visit cars_path

    within first '.car-row' do
      click_link 'Edit'
    end

    fill_in 'car[model]', with: 'Mercedes'
    fill_in 'car[registration_number]', with: 'PO 8899'
    click_button 'Save'

    assert has_content? 'Cars'
    assert has_content? 'Mercedes'
    assert has_content? 'PO 8899'
  end

  test 'user removes a car' do
    visit cars_path

    within first '.car-row' do
      click_link 'Remove'
    end

    assert has_content? 'Cars'
    assert_not has_content? 'Ford Mondeo'
    assert_not has_content? 'DW 5234'
  end

  test 'user adds car with an image bigger than expected' do
    visit new_car_path
    car_fill_form
    attach_file('car[image]', File.absolute_path('./test/fixtures/files/toyota_big.jpg'))
    click_button 'Save'

    assert has_content? 'Image should be less than 200 kb.'
  end

  test 'user removes an image from a car that exists' do
    visit cars_path
    within first '.car-row' do
      click_link 'Edit'
    end
    attach_file('car[image]', File.absolute_path('./test/fixtures/files/opel.jpg'))
    click_button 'Save'

    within first '.car-row' do
      click_link 'Show'
    end

    page.has_no_image?(src: './test/fixtures/files/opel.jpg')
  end
end
