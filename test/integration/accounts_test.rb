require 'test_helper'

class AccountsTest < ActionDispatch::IntegrationTest
  setup { ActionMailer::Base.deliveries.clear }

  test 'user creates new account' do
    test_sign_up

    assert has_content? 'Parkings'
    assert_equal '/en', current_path
    assert has_content? 'Current account: New User'
  end

  test 'send welcome email to new user' do
    user = Account.first
    RegistrationWelcome.send_signup_email(user).deliver_now
    deliveries = ActionMailer::Base.deliveries
    assert_equal 1, deliveries.count
    assert_equal 'Welcome to Bookparking', deliveries.first.subject
    assert_equal 'chris.black@black.com', deliveries.first.to.first
    assert_match /Hello Chris/, deliveries.first.to_s
  end

  test 'user creates tries to create account when passwords do not match' do
    visit signup_path
    fill_in 'E-mail:', with: 'dave.bream@bream.com'
    fill_in 'First name:', with: 'Dave'
    fill_in 'Last name:', with: 'Bream'
    fill_in 'Password:', with: 'correctpassword'
    fill_in 'Confirm assword:', with: 'wrongpassword'
    click_button 'Sign up'

    assert_equal '/en/register', current_path
    assert has_content? 'Sign up'
  end
end
