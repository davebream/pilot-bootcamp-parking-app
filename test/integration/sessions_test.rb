require 'test_helper'

class SessionsTest < ActionDispatch::IntegrationTest
  setup { OmniAuth.config.mock_auth[:facebook] = nil }

  test 'display user full name when signed in' do
    test_sign_in
    within '.user' do
      assert has_content? 'Chris Black'
    end
  end

  test 'display user full name only when signed in' do
    visit login_path
    assert has_no_content? 'Current account:'
  end

  test 'remove current user full name when sign out' do
    test_sign_in
    within '.user' do
      click_link 'Sign out'
    end
    assert has_no_content? 'Current account:'
  end

  test 'redirect to root if user signs in by going
  directly to sign in form' do
    visit login_path
    test_sign_in
    assert has_content? 'Parkings'
    assert_equal '/en', current_path
  end

  test 'redirect back if user was redirected to sign in form
  from another location' do
    visit place_rents_path
    test_sign_in
    assert has_content? 'Place Rents'
    assert_equal '/place_rents', current_path
  end

  test 'user signs in with facebook' do
    mock_auth_user
    visit root_path
    click_link 'Sign in with Facebook'
    assert has_content? 'Parkings'
    assert_equal '/en', current_path
    assert has_content? 'Current account: John Doe'
  end
end
