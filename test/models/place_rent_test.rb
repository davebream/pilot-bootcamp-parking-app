require 'test_helper'

class PlaceRentTest < ActiveSupport::TestCase
  setup do
    @rent = place_rents(:rent1)
  end

  test 'should be valid with correct data' do
    assert @rent.valid?
  end

  test 'should require starts at' do
    @rent.starts_at = nil
    assert @rent.invalid?
    assert_equal [:starts_at], @rent.errors.keys
  end

  test 'should require ends at' do
    @rent.ends_at = nil
    assert @rent.invalid?
    assert_equal [:ends_at], @rent.errors.keys
  end

  test 'should require car' do
    @rent.car_id = nil
    assert @rent.invalid?
    assert_equal [:car_id], @rent.errors.keys
  end

  test 'calculate price when end hour is earlier than start hour' do
    @rent.starts_at = '2015-07-08 07:00:00 UTC'
    @rent.ends_at = '2015-07-12 05:00:00 UTC'
    assert_equal '400.0', @rent.calculate_price.to_s
  end

  test 'calculate price seconds after new hour is started' do
    @rent.starts_at = '2015-07-08 07:00:00 UTC'
    @rent.ends_at = '2015-07-12 07:00:10 UTC'
    assert_equal '250.0', @rent.calculate_price.to_s
  end

  test 'calculate price when rent is longer than a month' do
    @rent.starts_at = '2015-07-08 07:00:00 UTC'
    @rent.ends_at = '2015-08-08 07:00:00 UTC'
    assert_equal '1860.0', @rent.calculate_price.to_s
  end

  test 'calculate price when rent time is less than an hour' do
    @rent.starts_at = '2015-07-08 07:00:00 UTC'
    @rent.ends_at = '2015-07-08 07:00:10 UTC'
    assert_equal '10.0', @rent.calculate_price.to_s
  end

  test 'should change rents end date to now' do
    rent = PlaceRent.create(starts_at: DateTime.now - 48.hours,
                            ends_at: DateTime.now + 48.hours,
                            parking: @rent.parking, car: @rent.car)
    rent.finish
    assert(rent.ends_at < DateTime.now)
  end
end
