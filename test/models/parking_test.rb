require 'test_helper'

class ParkingTest < ActiveSupport::TestCase
  setup do
    @parking = parkings(:parking1)
  end

  test 'should be valid with correct data' do
    assert @parking.valid?
  end

  test 'should require number of places' do
    @parking.places = nil
    assert @parking.invalid?
    assert_equal [:places], @parking.errors.keys
  end

  test 'should require hour_price' do
    @parking.hour_price = nil
    assert @parking.invalid?
    assert_equal [:hour_price], @parking.errors.keys
  end

  test 'should require day_price' do
    @parking.day_price = nil
    assert @parking.invalid?
    assert_equal [:day_price], @parking.errors.keys
  end

  test 'should require kind from the given list' do
    @parking.kind = 'underground'
    assert @parking.invalid?
    assert_equal [:kind], @parking.errors.keys
  end

  test 'should require hour_price not be a string' do
    @parking.hour_price = 'string'
    assert @parking.invalid?
    assert_equal [:hour_price], @parking.errors.keys
  end

  test 'should require day_price not be a string' do
    @parking.day_price = 'string'
    assert @parking.invalid?
    assert_equal [:day_price], @parking.errors.keys
  end

  test 'should finish rents for parking when parking is destroyed' do
    rent = @parking.place_rents.first
    rent.starts_at = DateTime.now - 48.hours
    rent.ends_at = DateTime.now + 48.hours

    @parking.destroy
    rent.reload
    assert(rent.ends_at <= DateTime.now)
  end

  test 'should return only private parkings' do
    parkings = Parking.in_private

    assert_equal 8, parkings.count
    assert_equal %w(Poznań), parkings.map { |p| p.address.city }.uniq
  end

  test 'should return only public parkings' do
    parkings = Parking.in_public

    assert_equal 3, parkings.count
    assert_equal %w(Wrocław Warszawa Rzeszów), parkings.map { |p| p.address.city }
  end

  test 'should return parkings with day_price between range' do
    parkings = Parking.day_price_between(45, 65)

    assert_equal 2, parkings.count
    assert_equal %w(Wrocław Poznań), parkings.map { |p| p.address.city }
  end

  test 'should return parkings with hour_price between range' do
    parkings = Parking.hour_price_between(5, 11)

    assert_equal 2, parkings.count
    assert_equal %w(Wrocław Poznań), parkings.map { |p| p.address.city }
  end

  test 'should return parkings in the given city' do
    parkings = Parking.in_city('Wrocław')

    assert_equal 1, parkings.count
    assert_equal %w(Wrocław), parkings.map { |p| p.address.city }
  end
end
