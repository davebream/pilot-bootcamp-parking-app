require 'test_helper'

class CarTest < ActiveSupport::TestCase
  setup do
    @car = cars(:ford)
  end

  test 'should be valid with correct data' do
    assert @car.valid?
  end

  test 'should require registration number' do
    @car.registration_number = nil
    assert @car.invalid?
    assert_equal [:registration_number], @car.errors.keys
  end

  test 'should require model' do
    @car.model = nil
    assert @car.invalid?
    assert_equal [:model], @car.errors.keys
  end

  test 'should require owner' do
    @car.owner_id = nil
    assert @car.invalid?
    assert_equal [:owner_id], @car.errors.keys
  end
end
