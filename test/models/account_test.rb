require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  test 'should authenticate if credentials are ok' do
    assert Account.authenticate('chris.black@black.com', 'correctpassword')
  end

  test 'should authenticate only if credentials are ok' do
    assert_not Account.authenticate('wrong@wrong.com', 'wrongpassword')
  end

  test 'should authenticate only if the password is correct' do
    assert_not Account.authenticate('chris.black@black.com', 'wrongpassword')
  end

  test 'should authenticate only if the email is correct' do
    assert_not Account.authenticate('wrong@wrong.com', 'correctpassword')
  end
end
