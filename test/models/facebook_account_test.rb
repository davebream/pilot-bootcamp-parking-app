require 'test_helper'

class FacebookAccountTest < ActiveSupport::TestCase
  setup { OmniAuth.config.mock_auth[:facebook] = nil }

  test 'should find facebook acount if exists' do
    account = FacebookAccount.find_or_create_for_facebook(mock_auth_user)
    assert_equal '12345678', account.uid
  end

  test 'should create facebook acount if does not exist' do
    account = FacebookAccount.find_or_create_for_facebook(mock_auth_new_user)
    assert_equal '87654321', account.uid
  end
end
