require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  setup do
    @person = people(:john)
  end

  test 'should be valid with correct data' do
    assert @person.valid?
  end

  test 'should require first name' do
    @person.first_name = nil
    assert @person.invalid?
    assert_equal [:first_name], @person.errors.keys
  end
end
