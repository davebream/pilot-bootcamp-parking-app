require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  setup do
    @address = addresses(:wroclaw)
  end

  test 'should be valid with correct data' do
    assert @address.valid?
  end

  test 'should require city' do
    @address.city = nil
    assert @address.invalid?
    assert_equal [:city], @address.errors.keys
  end

  test 'should require street' do
    @address.street = nil
    assert @address.invalid?
    assert_equal [:street], @address.errors.keys
  end

  test 'should require zip code' do
    @address.zip_code = nil
    assert @address.invalid?
    assert_equal [:zip_code], @address.errors.keys
  end

  test 'should require zip code to have no letters' do
    @address.zip_code = 'nn-str'
    assert @address.invalid?
    assert_equal [:zip_code], @address.errors.keys
  end

  test 'should require 6 characters in the zip code' do
    @address.zip_code = '12-4567'
    assert @address.invalid?
    assert_equal [:zip_code], @address.errors.keys
  end

  test 'should require hyphen as the third character in the zip code' do
    @address.zip_code = '090903'
    assert @address.invalid?
    assert_equal [:zip_code], @address.errors.keys
  end
end
