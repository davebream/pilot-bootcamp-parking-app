require 'test_helper'
require 'capybara/rails'

class ApplicationHelperTest < ActiveSupport::TestCase
  include ApplicationHelper

  attr_reader :controller_name

  test 'use controller name as a page title' do
    @controller_name = 'mock'
    assert_equal 'Mock', page_title
  end

  test 'require humanized controller name as a page title' do
    @controller_name = 'mock_for_tests'
    assert_equal 'Mock for tests', page_title
  end
end
