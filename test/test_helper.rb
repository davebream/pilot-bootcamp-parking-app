ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  # Add more helper methods to be used by all tests here...
  OmniAuth.config.test_mode = true

  def mock_auth_user
    OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new(
      provider: 'facebook',
      uid: '12345678',
      info: {
        name: 'John Doe'
      }
    )
  end

  def mock_auth_new_user
    OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new(
      provider: 'facebook',
      uid: '87654321',
      info: {
        name: 'Mark Black'
      }
    )
  end
end

class ActionDispatch::IntegrationTest
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL

  setup do
    Capybara.reset_session!
  end

  OmniAuth.config.mock_auth[:facebook] = OmniAuth::AuthHash.new(
    provider: 'facebook',
    uid: '12345678',
    name: 'John Doe'
  )

  private

  def test_sign_in
    visit login_path

    fill_in 'email', with: 'chris.black@black.com'
    fill_in 'password', with: 'correctpassword'
    click_button 'Sign in'
  end

  def test_sign_up
    visit signup_path

    fill_in 'E-mail:', with: 'new@new.com'
    fill_in 'First name:', with: 'New'
    fill_in 'Last name:', with: 'User'
    fill_in 'Password:', with: '1234'
    fill_in 'Confirm assword:', with: '1234'
    click_button 'Sign up'
  end

  def car_fill_form
    fill_in 'car[model]', with: 'Audi'
    fill_in 'car[registration_number]', with: 'WA 6655'
  end
end
