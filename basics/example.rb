class Article
  def initialize(title, body, author = nil)
    @created_at = Time.now
    @title = title
    @body = body
    @author = author
    self.likes = 0
    self.dislikes = 0
  end

  attr_reader :title, :body, :author, :created_at
  attr_accessor :likes, :dislikes

  def like!
    self.likes += 1
  end

  def dislike!
    self.dislikes += 1
  end

  def points
    likes - dislikes
  end

  def votes
    likes + dislikes
  end

  def long_lines
    body.lines.select { |line| line.length > 80 }
  end

  def length
    body.length
  end

  def truncate(limit)
    if body.length > limit
      body[0...(limit - 3)] + '...'
    else
      body
    end
  end

  def contain?(query)
    !!body.index(query)
  end
end
class ArticlesFileSystem
  def initialize(directory_name)
    @directory = directory_name
  end

  def save(articles)
    articles.each do |a|
      file_name = a.title.downcase.tr(' ', '_')
      file_content = [a.author, a.likes.to_s, a.dislikes.to_s, a.body].join('||')
      IO.write(File.join(@directory, file_name + '.article'), file_content)
    end
  end

  def load
    Dir.glob("#{@directory}/*.article").map do |filename|
      file_content = File.read(filename)
      article_array = file_content.split('||')
      file_basename = File.basename(filename, '.article').capitalize.tr('_', ' ')

      article = Article.new(file_basename, article_array[3], article_array[0])
      article.likes = article_array[1].to_i
      article.dislikes = article_array[2].to_i
      article
    end
  end
end

class WebPage
  def initialize(directory = '/')
    @filesystem = ArticlesFileSystem.new(directory)
    load
  end

  class NoArticlesFound < StandardError
  end

  attr_reader :articles

  def load
    @articles = @filesystem.load
  end

  def save
    @filesystem.save(@articles)
  end

  def new_article(title, body, author)
    article = Article.new(title, body, author)
    @articles.push(article)
  end

  def longest_articles
    @articles.sort_by { |article| article.length }.reverse
  end

  def best_articles
    @articles.sort_by { |article| article.points }.reverse
  end

  def worst_articles
    best_articles.reverse
  end

  def best_article
    raise NoArticlesFound if @articles.empty?
    best_articles.first
  end

  def worst_article
    raise NoArticlesFound if @articles.empty?
    worst_articles.first
  end

  def most_controversial_articles
    @articles.sort_by { |article| article.votes }.reverse
  end

  def votes
    @articles.map(&:votes).inject(0, &:+)
  end

  def authors
    @articles.map(&:author).uniq
  end

  def authors_statistics
    @articles.each_with_object(Hash.new(0)) { |article, counts| counts[article.author] += 1 }
  end

  def best_author
    authors_statistics.max_by { |_, articles| articles }.first
  end

  def search(query)
    @articles.select { |article| article.contain?(query) }
  end
end
