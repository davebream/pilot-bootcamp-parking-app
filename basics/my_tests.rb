require 'minitest/autorun'
require './example'

class ArticleTest < Minitest::Test
  def test_initialization
    article = Article.new('Title', 'Body', 'Author')

    assert_equal('Title', article.title)
    assert_equal('Body', article.body)
    assert_equal('Author', article.author)
    assert_in_delta(Time.now, article.created_at, 1.0)
    assert_equal(0, article.likes)
    assert_equal(0, article.dislikes)
  end

  def test_initialization_with_anonymous_author
    article = Article.new('Title', 'Body')

    assert_equal('Title', article.title)
    assert_equal('Body', article.body)
    assert_nil(article.author)
    assert_equal(0, article.likes)
    assert_equal(0, article.dislikes)
  end

  def test_liking
    article = Article.new('Title', 'Body', 'Author')
    30.times { article.like! }

    assert_equal(30, article.likes)
  end

  def test_disliking
    article = Article.new('Title', 'Body', 'Author')
    4.times { article.dislike! }

    assert_equal(4, article.dislikes)
  end

  def test_points
    article = Article.new('Title', 'Body', 'Author')
    30.times { article.like! }
    20.times { article.dislike! }

    assert_equal(10, article.points)
  end

  def test_long_lines
    test_body1 = "Body \n" * 2
    test_body2 = "Body line that is longer than 80 characters. Body line that is longer than 80 characters. Body line that is longer than 80 characters. \n" * 30
    test_body3 = "Body line that is not longer than 80 characters. \n" * 10
    test_body = test_body1 + test_body2 + test_body3
    article = Article.new('Title', test_body, 'Author')

    assert_equal(test_body2.lines, article.long_lines, "The returned array doesn't contain longest lines.")
  end

  def test_truncate
    article = Article.new('Title', 'This is a quite long Body', 'Author')

    assert_equal('This i...', article.truncate(9), 'Article body was truncated incorrectly.')
  end

  def test_truncate_when_limit_is_longer_then_body
    article = Article.new('Title', 'Body test here', 'Author')

    assert_equal('Body test here', article.truncate(15), 'Article body was truncated incorrectly.')
  end

  def test_truncate_when_limit_is_same_as_body_length
    article = Article.new('Title', 'Body', 'Author')

    assert_equal('Body', article.truncate(4), 'Article body was truncated incorrectly.')
  end

  def test_length
    article = Article.new('Title', 'Test Body', 'Author')

    assert_equal(9, article.length)
  end

  def test_votes
    article = Article.new('Title', 'Body', 'Author')
    30.times { article.like! }
    20.times { article.dislike! }

    assert_equal(50, article.votes)
  end

  def test_contain
    article = Article.new('Title', 'This is a Body of the article', 'Author')

    assert_equal(true, article.contain?('is'))
    assert_equal(true, article.contain?(/[Bb]ody/))
    assert_equal(false, article.contain?('Author'))
  end
end
class ArticlesFileSystemTest < Minitest::Test
  def setup
    @directory = Dir.mktmpdir
    @filesystem = ArticlesFileSystem.new(@directory)
  end

  def teardown
    FileUtils.remove_entry @directory
  end

  def test_saving
    articles = []

    article1 = Article.new('Title for Article1', 'Body Article1', 'Author of Article1')
    article2 = Article.new('Title for Article2', 'Body Article2', 'Author of Article2')
    articles.push(article1, article2)
    @filesystem.save(articles)

    assert_equal(true, File.exist?(File.join(@directory, 'title_for_article1.article')), 'The file was not found in the given directory')
    assert_equal(true, File.exist?(File.join(@directory, 'title_for_article2.article')), 'The file was not found in the given directory')

    file_name = File.join(@directory, 'title_for_article1.article')
    file_basename = File.basename(file_name, '.article').downcase.tr('_', ' ')
    file_content = IO.read(file_name).split('||')

    assert_equal('Title for Article1'.downcase, file_basename)
    assert_equal('Body Article1', file_content[3])
    assert_equal('Author of Article1', file_content[0])
    assert_equal(0, file_content[1].to_i)
    assert_equal(0, file_content[2].to_i)
  end

  def test_loading
    IO.write(File.join(@directory, 'article_about_a_mechanic.article'), 'Jase Mcguire||10||12||Debbie has become a car mechanic.')
    IO.write(File.join(@directory, 'article_about_a_singer.article'), 'Sebastian Young||2||5||Miss Johnson is a singer.')
    IO.write(File.join(@directory, 'article_about_a_scientist.article'), "Kyree Rodriquez||9||3||Joe's girlfriend will become a scientist.")
    IO.write(File.join(@directory, 'article_about_a_scientist.art'), "Kyree Rodriquez||9||3||Joe's girlfriend will become a scientist.")

    articles = @filesystem.load
    assert_equal(3, articles.length)

    articles.sort! { |a, b| a.title <=> b.title }

    article = articles[2]

    assert_equal('Article about a singer', article.title)
    assert_equal('Miss Johnson is a singer.', article.body)
    assert_equal('Sebastian Young', article.author)
    assert_equal(2, article.likes.to_i)
    assert_equal(5, article.dislikes.to_i)
  end
end
class WebPageTest < Minitest::Test
  def setup
    @directory = Dir.mktmpdir
    @webpage = WebPage.new(@directory)
    example_articles
  end

  def teardown
    FileUtils.remove_entry @directory
  end

  def test_new_without_anything_to_load
    webpage = Dir.exist?('nothing_to_load') ? WebPage.new('nothing_to_load') : WebPage.new(Dir.mkdir('nothing_to_load'))

    assert_equal(0, webpage.articles.length)

    Dir.rmdir('nothing_to_load')
  end

  def test_new_article
    assert_equal(3, @webpage.articles.length)

    @webpage.new_article('Article about a scientist.', "Joe's girlfriend will become a scientist.", 'Kyree Rodriquez')

    assert_equal(4, @webpage.articles.length)
  end

  def test_longest_articles
    assert_equal('Kieran Porter', @webpage.longest_articles[0].author)
    assert_equal('Tom Wilson', @webpage.longest_articles[1].author)
    assert_equal('Jamie Watts', @webpage.longest_articles[2].author)
  end

  def test_best_articles
    assert_equal('Kieran Porter', @webpage.best_articles[0].author)
    assert_equal('Jamie Watts', @webpage.best_articles[1].author)
    assert_equal('Tom Wilson', @webpage.best_articles[2].author)
  end

  def test_best_article
    assert_equal('Kieran Porter', @webpage.best_article.author)
  end

  def test_best_article_exception_when_no_articles_can_be_found
    @webpage.articles.clear
    assert_raises(WebPage::NoArticlesFound) { @webpage.best_article }
  end

  def test_worst_articles
    assert_equal('Tom Wilson', @webpage.worst_articles[0].author)
    assert_equal('Jamie Watts', @webpage.worst_articles[1].author)
    assert_equal('Kieran Porter', @webpage.worst_articles[2].author)
  end

  def test_worst_article
    assert_equal('Tom Wilson', @webpage.worst_article.author)
  end

  def test_worst_article_exception_when_no_articles_can_be_found
    @webpage.articles.clear
    assert_raises(WebPage::NoArticlesFound) { @webpage.worst_article }
  end

  def test_most_controversial_articles
    assert_equal('Jamie Watts', @webpage.most_controversial_articles[0].author)
    assert_equal('Tom Wilson', @webpage.most_controversial_articles[1].author)
    assert_equal('Kieran Porter', @webpage.most_controversial_articles[2].author)
  end

  def test_votes
    assert_equal(55, @webpage.votes)
  end

  def test_authors
    article = Article.new('Extra Article', "Paul's grandson is a computer programmer.", 'Jamie Watts')
    @webpage.articles.push(article)

    assert_equal(3, @webpage.authors.length, 'Wrong authors count')
  end

  def test_authors_statistics
    article = Article.new('Extra Article', "Paul's grandson is a computer programmer.", 'Jamie Watts')
    @webpage.articles.push(article)

    assert_equal({ 'Jamie Watts' => 2, 'Tom Wilson' => 1, 'Kieran Porter' => 1 }, @webpage.authors_statistics)
  end

  def test_best_author
    article = Article.new('Extra Article', "Paul's grandson is a computer programmer.", 'Jamie Watts')
    @webpage.articles.push(article)

    assert_equal('Jamie Watts', @webpage.best_author)
  end

  def test_search
    article = Article.new('Extra Article', "Paul's grandson is a computer programmer.", 'Jamie Watts')
    @webpage.articles.push(article)

    assert_equal('Jamie Watts', @webpage.search('computer').first.author)
    assert_equal('Jamie Watts', @webpage.search(/[Cc]omputer/).first.author)
  end

  private

  def example_articles
    article1 = Article.new('Hungry people', "He isn't hungry.", 'Jamie Watts')
    10.times { article1.like! }
    12.times { article1.dislike! }

    article2 = Article.new('Spy game', "Marge didn't become a spy.", 'Tom Wilson')
    2.times { article2.like! }
    20.times { article2.dislike! }

    article3 = Article.new('Taxi drivers in the town', "Mrs. O'Day has become a taxi driver.", 'Kieran Porter')
    11.times { article3.like! }
    0.times { article3.dislike! }

    @webpage.articles.push(article1, article2, article3)
  end
end
