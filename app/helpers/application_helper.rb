module ApplicationHelper
  def page_title
    controller_name.humanize
  end

  def flash_class(type)
    case type
    when 'notice' then 'alert alert-info'
    when 'success' then 'alert alert-success'
    when 'error' then 'alert alert-warning'
    when 'alert' then 'alert alert-danger'
    end
  end
end
