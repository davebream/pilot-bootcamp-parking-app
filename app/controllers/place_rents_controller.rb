class PlaceRentsController < ApplicationController
  before_action :authorize_user

  def index
    @place_rents = current_person.place_rents
  end

  def show
    @place_rent = current_person.place_rents.find_by(unique_id: params[:id])
  end

  def new
    @place_rent = PlaceRent.new(parking: parking)
  end

  def create
    @place_rent = current_person.place_rents.new(place_rent_params)
    @place_rent.parking = parking
    if @place_rent.save
      redirect_to @place_rent
    else
      render 'new'
    end
  end

  private

  def place_rent_params
    params.require(:place_rent).permit(:starts_at, :ends_at, :car_id)
  end

  def parking
    @parking ||= Parking.find(params[:parking_id])
  end
end
