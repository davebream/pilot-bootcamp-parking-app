class SessionsController < ApplicationController
  def new
  end

  def create
    if request.env['omniauth.auth'] && request.env['omniauth.auth'][:uid]
      account = FacebookAccount.find_or_create_for_facebook(request.env['omniauth.auth'])
      facebook_sign_in(account)
    else
      account = Account.authenticate(params[:email], params[:password])
      sign_in(account)
    end
  end

  def destroy
    reset_session
    redirect_to root_path
  end
end
