class CarsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :car_not_found
  before_action :authorize_user
  before_action :car, only: [:show, :edit]

  def index
    @cars = current_person.cars.order(:id)
  end

  def new
    @car = Car.new
  end

  def create
    @car = current_person.cars.new(car_params)
    if car.save
      redirect_to cars_path
    else
      render :new
    end
  end

  def update
    if car.update_attributes(car_params)
      redirect_to cars_path
    else
      render :edit
    end
  end

  def destroy
    car.destroy
    redirect_to cars_path
  end

  private

  def car
    @car ||= current_person.cars.find(params[:id])
  end

  def car_not_found
    redirect_to cars_path, notice: 'Car does not exist!'
  end

  def car_params
    params.require(:car).permit(:registration_number, :model, :image, :remove_image, :owner_id)
  end
end
