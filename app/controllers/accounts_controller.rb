class AccountsController < ApplicationController
  def new
    @account = Account.new
    @account.build_person
  end

  def create
    @account = Account.new(account_params)
    if @account.save
      RegistrationWelcome.send_signup_email(@account).deliver_now
      sign_in(@account)
    else
      render :new
    end
  end

  private

  def account_params
    params.require(:account).permit(:email,
                                    :password,
                                    :password_confirmation,
                                    person_attributes: [
                                      :first_name, :last_name
                                    ])
  end
end
