class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || extract_locale_from_header
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

  def extract_locale_from_header
    locales = I18n.available_locales.map(&:to_s)
    language = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first if request.env['HTTP_ACCEPT_LANGUAGE']
    locales.include?(language) ? language : I18n.default_locale
  end

  helper_method :current_person
  helper_method :current_account
  helper_method :user_signed_in?

  private

  def current_person
    Person.find_by(id: session[:person_id])
  end

  def authorize_user
    return unless current_person.nil?
    session[:return_to] = request.url
    redirect_to login_path, notice: 'Please sign in!'
  end

  def user_signed_in?
    !current_person.nil?
  end

  def sign_in(account)
    if account
      session[:person_id] = account.person_id
      redirect_to session.delete(:return_to) || root_path
    else
      flash.now[:error] = 'Wrong credentials'
      render :new
    end
  end

  def facebook_sign_in(account)
    if account && account.person_id
      session[:person_id] = account.person_id
      redirect_to root_path
    else
      flash.now[:error] = 'Could not login with Facebook'
      render :new
    end
  end
end
