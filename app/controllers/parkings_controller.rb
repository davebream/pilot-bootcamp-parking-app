class ParkingsController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, with: :parking_not_found

  def index
    if params
      @parkings = Parking.search(params).order(:id).page(params[:page])
    else
      @parkings = Parking.order(:id).page(params[:page])
    end

    respond_to do |format|
      format.html
      format.json do
        render json: @parkings
      end
    end
  end

  def show
    parking
    respond_to do |format|
      format.html
      format.json do
        render json: @parking
      end
    end
  end

  def new
    @parking = Parking.new
    parking.build_address
  end

  def create
    @parking = Parking.new(parking_params)
    if parking.save
      redirect_to parkings_path
    else
      render :new
    end
  end

  def edit
    parking
    parking.build_address if parking.address.nil?
  end

  def update
    if parking.update(parking_params)
      redirect_to parkings_path
    else
      render :edit
    end
  end

  def destroy
    parking.destroy
    redirect_to parkings_path
  end

  private

  def parking
    @parking ||= Parking.find(params[:id])
  end

  def parking_not_found
    redirect_to parkings_path, notice: t(:parking_not_found)
  end

  def parking_params
    params.require(:parking).permit(:places,
                                    :kind,
                                    :hour_price,
                                    :day_price,
                                    address_attributes: [
                                      :city,
                                      :street,
                                      :zip_code
                                    ])
  end
end
