class RegistrationWelcome < ApplicationMailer
  default from: 'hello@bookparking.dev'
  layout 'mailer'

  def send_signup_email(account)
    @account = account
    @user = account.person
    mail(
      to: @account.email,
      subject: 'Welcome to Bookparking'
    )
  end
end
