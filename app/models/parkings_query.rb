class ParkingsQuery
  def initialize(query)
    @parkings = Parking.all
    @query = query
    @min_price = 0
    @max_price = Float::INFINITY
  end

  def results
    private_public_filter
    day_price_filter
    hour_price_filter
    city_filter
  end

  def private_public_filter
    if @query[:private].present? && @query[:public].present?
      @parkings
    elsif @query[:private].present? || @query[:public].present?
      @parkings = @parkings.in_private if @query[:private].present?
      @parkings = @parkings.in_public if @query[:public].present?
    else
      @parkings
    end
  end

  def day_price_filter
    if @query[:day_price_from].present? || @query[:day_price_to].present?
      min = @query[:day_price_from].empty? ? @min_price : @query[:day_price_from]
      max = @query[:day_price_to].empty? ? @max_price : @query[:day_price_to]
      @parkings = @parkings.day_price_between(min.to_f, max.to_f)
    else
      @parkings
    end
  end

  def hour_price_filter
    if @query[:hour_price_from].present? || @query[:hour_price_to].present?
      min = @query[:hour_price_from].empty? ? @min_price : @query[:hour_price_from]
      max = @query[:hour_price_to].empty? ? @max_price : @query[:hour_price_to]
      @parkings = @parkings.hour_price_between(min.to_f, max.to_f)
    else
      @parkings
    end
  end

  def city_filter
    if @query[:city].present?
      @parkings = @parkings.in_city(@query[:city])
    else
      @parkings
    end
  end
end
