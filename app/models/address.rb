class Address < ActiveRecord::Base
  validates :city, :street, :zip_code, presence: true
  validates_format_of :zip_code,
                      with: /\A\d{2}\-\d{3}\z/,
                      message: :message

  has_one :parking
end
