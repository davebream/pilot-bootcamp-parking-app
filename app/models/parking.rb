class Parking < ActiveRecord::Base
  before_destroy :finish_rents
  KINDS = %w(outdoor indoor private street)

  scope :in_private, -> { where(kind: 'private') }
  scope :in_public, -> { where.not(kind: 'private') }

  validates :places, :hour_price, :day_price, presence: true
  validates :kind, inclusion: { in: KINDS }
  validates :hour_price, :day_price, numericality: true

  belongs_to :address
  belongs_to :owner, class_name: 'Person'
  has_many :place_rents, dependent: :nullify

  accepts_nested_attributes_for :address

  paginates_per 10

  def as_json(options = {})
    super(
      except: [:owner_id, :address_id],
      include: { address: { only: [:id, :street, :city, :zip_code] } }
    )
  end

  def self.day_price_between(from, to)
    where(day_price: from..to)
  end

  def self.hour_price_between(from, to)
    where(hour_price: from..to)
  end

  def self.in_city(city)
    joins(:address).where('city like ?', city)
  end

  def self.search(params)
    ParkingsQuery.new(params).results
  end

  private

  def finish_rents
    place_rents.unfinished.each(&:finish)
  end
end
