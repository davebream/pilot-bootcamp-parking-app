class Person < ActiveRecord::Base
  validates :first_name, presence: true

  has_many :cars, foreign_key: 'owner_id'
  has_many :parkings, foreign_key: 'owner_id'
  has_many :place_rents, through: :cars

  def full_name
    [first_name, last_name].compact.join(' ')
  end
end
