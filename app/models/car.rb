class Car < ActiveRecord::Base
  extend Dragonfly::Model
  extend Dragonfly::Model::Validations
  dragonfly_accessor :image

  validates :registration_number, :model, :owner_id, presence: true
  validates_size_of :image, maximum: 200.kilobytes, message: 'should be less than 200 kb.'
  validates_property :format, of: :image, in: %w(jpeg jpg png gif)

  belongs_to :owner, class_name: 'Person'
  has_many :place_rents, dependent: :delete_all

  def to_param
    [id, model.parameterize].join("-")
  end
end
