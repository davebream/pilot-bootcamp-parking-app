class FacebookAccount < ActiveRecord::Base
  validates :uid, :person, presence: true
  belongs_to :person

  def self.find_or_create_for_facebook(auth_hash)
    where(uid: auth_hash[:uid]).first_or_create do |account|
      account.uid = auth_hash[:uid]
      account.build_person(
        first_name: auth_hash.info.name.split.first,
        last_name: auth_hash.info.name.split.last
      )
    end
  end
end
