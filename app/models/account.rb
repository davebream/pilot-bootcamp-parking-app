class Account < ActiveRecord::Base
  validates :email, :password, :password_confirmation, presence: true
  belongs_to :person
  has_secure_password
  accepts_nested_attributes_for :person

  def self.authenticate(email, password)
    account = Account.find_by(email: email)
    account.authenticate(password) if account
  end
end
