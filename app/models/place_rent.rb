class PlaceRent < ActiveRecord::Base
  validates :starts_at, :ends_at, :car_id, presence: true
  before_save :set_price
  before_create :generate_random_id

  scope :unfinished, -> { where('ends_at > ?', DateTime.now) }

  belongs_to :car
  belongs_to :parking
  has_one :owner, through: :car

  def to_param
    unique_id
  end

  def calculate_price
    days = (ends_at.to_i - starts_at.to_i) / 1.days
    hours = ((ends_at.to_i - starts_at.to_i) / 1.hour.to_f).ceil - days * 24
    parking.day_price * days + parking.hour_price * hours
  end

  def set_price
    self.price = calculate_price
  end

  def finish
    update_attributes(ends_at: DateTime.now)
  end

  private

  def generate_random_id
    self.unique_id = SecureRandom.uuid
  end
end
