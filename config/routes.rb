Rails.application.routes.draw do

  scope '(:locale)', locale: /en|pl/ do
    root 'parkings#index'
    resources :parkings do
      resources :place_rents, only: [:new, :create]
    end

    resources :cars
    resources :place_rents, only: [:index, :show]

    get 'login', to: 'sessions#new', as: :login
    post 'login', to: 'sessions#create', as: :session
    delete 'logout', to: 'sessions#destroy', as: :logout
    get 'register', to: 'accounts#new', as: :signup
    post 'register', to: 'accounts#create', as: :accounts
  end

  #get '/:locale', to: 'parkings#index'
  root 'parkings#index', as: nil
  resource :session

  get '/auth/:provider/callback', to: 'sessions#create'
  get '/auth/:provider', to: 'sessions#create', as: 'auth_sign_in'
  get '/auth/failure', to: 'sessions#failure'
end
