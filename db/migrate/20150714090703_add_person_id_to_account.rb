class AddPersonIdToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :person_id, :integer
  end
end
