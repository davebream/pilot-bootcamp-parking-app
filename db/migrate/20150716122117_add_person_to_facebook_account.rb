class AddPersonToFacebookAccount < ActiveRecord::Migration
  def change
    add_column :facebook_accounts, :person_id, :integer
    add_index :facebook_accounts, :person_id
  end
end
