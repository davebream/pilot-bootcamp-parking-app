class AddForeignKeysToModels < ActiveRecord::Migration
  def change
    add_foreign_key :cars, :people, column: :owner_id, primary_key: 'id'
    add_foreign_key :parkings, :people, column: :owner_id, primary_key: 'id'
    add_foreign_key :parkings, :addresses
    add_foreign_key :place_rents, :parkings
    add_foreign_key :place_rents, :cars
  end
end
