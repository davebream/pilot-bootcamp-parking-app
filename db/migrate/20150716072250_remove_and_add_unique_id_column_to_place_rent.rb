class RemoveAndAddUniqueIdColumnToPlaceRent < ActiveRecord::Migration
  def change
    remove_index :place_rents, :unique_id
    add_index :place_rents, :unique_id, unique: true
  end
end
