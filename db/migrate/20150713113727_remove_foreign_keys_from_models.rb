class RemoveForeignKeysFromModels < ActiveRecord::Migration
  def change
    remove_foreign_key :place_rents, :parkings
  end
end
