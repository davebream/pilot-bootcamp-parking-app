class RemoveImageTitleColumnFromCar < ActiveRecord::Migration
  def change
    remove_column :cars, :image_name
  end
end
