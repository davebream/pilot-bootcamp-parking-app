class AddUniqueIdToPlaceRent < ActiveRecord::Migration
  def change
    add_column :place_rents, :unique_id, :string
    add_index :place_rents, :unique_id
  end
end
