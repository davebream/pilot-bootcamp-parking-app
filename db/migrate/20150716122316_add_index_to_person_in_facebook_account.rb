class AddIndexToPersonInFacebookAccount < ActiveRecord::Migration
  def change
    add_index :facebook_accounts, :uid, unique: true
  end
end
